#include <iostream>

using namespace std;

bool match(const string& pattern, const string& s) {
    for(int i = 0, k = 0; i < pattern.length(); i++, k++) {
        if(k < s.length()) {
            if(pattern[i] == s[k] || pattern[i] == '?') continue;
            else if(pattern[i] == '*') {
                if(i-1 == pattern.length()) continue;
                else {
                    for(int j = i+1; j < s.length(); j++){
                            if(s[j]!=pattern[i+1]) {
                                k++;
                            }
                                else {
                                k++;
                                i++;
                                break;
                                }
                    }
                }
        }
        else return 0;
    }
    else if((s[k-1] != pattern[i]) ) return 0;
    }
    return 1;
}

int main() {
    string s;
    string pattern;
    cout << "Podaj szablon: ";
    cin >> pattern;
    cout << "Podaj lancuch znakow: ";
    cin >> s;
    if(match(pattern, s)) cout << "Lancuch zgodny ze wzorcem.";
    else cout << "Lancuch niezgodny ze wzorcem.";
    return 0;
}
