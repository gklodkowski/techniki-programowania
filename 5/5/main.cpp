#include <iostream>

using namespace std;

class VectorInt {
public:
    int *integers;
    int numberOfIntegers;
    int sizeOfArray;
    VectorInt() {
        integers = new int[16];
        for(int i = 0; i < 16; i++)
            integers[i] = NULL;
        sizeOfArray = 16;
    }
    VectorInt(int numberOfIntegers) {
        integers = new int[numberOfIntegers];
            for(int i = 0; i < numberOfIntegers; i++)
                integers[i] = NULL;
            sizeOfArray = numberOfIntegers;
    }
    VectorInt(const VectorInt &vectorInt) {
        integers = vectorInt.integers;
    }
    VectorInt operator =(const VectorInt &vectorInt) {
        if(&vectorInt != this) {
            integers = vectorInt.integers;
        }
        return *this;
    }
    ~VectorInt() {
        delete [] integers;
    }
    int at(int index) {
        return integers[index];
    }
    void insert(int index, int value) {
        integers[index] = value;
    }
    void shrinkToFit() {
        if(sizeOfArray > 1) {
            int counter;
            for(counter = 0; integers[counter] != NULL; counter++);
            int *tempArray = new int[counter];
            for(int i  = 0; i < counter; i++)
                tempArray[i] = integers[i];
            delete [] integers;
            integers = new int[counter];
            for(int i  = 0; i < counter; i++)
                integers[i] = tempArray[i];
            delete [] tempArray;
            sizeOfArray=counter;
            integers[sizeOfArray] = NULL;
        }
    }
    int size() {
        int counter;
        for(counter = 0; integers[counter] != NULL; counter++);
        return counter;
    }
    void popBack() {
        integers[size()-1] = NULL;
    }
    void pushBack(int value) {
        if(integers[sizeOfArray-1] == NULL) {
            integers[size()] = value;
            integers[size()+1] = NULL;
        }
        else {
            int *tempArray = new int[sizeOfArray];
            for(int i  = 0; i < sizeOfArray; i++)
                tempArray[i] = integers[i];
            delete [] integers;
            integers = new int[sizeOfArray+1];
            for(int i  = 0; i < sizeOfArray; i++)
                integers[i] = tempArray[i];
            delete [] tempArray;
            integers[sizeOfArray] = value;
            integers[sizeOfArray+1] = NULL;
            sizeOfArray++;
        }
    }
    int capacity() {
        return sizeOfArray-size();
    }
    void clear() {
        for(int i = size(); i >= 0; i--)
            integers[i] = NULL;
    }
};
ostream& operator<<(ostream& out, const VectorInt& vectorInt) {
        for(int i = 0; i < vectorInt.sizeOfArray; i++)
            out << vectorInt.integers[i] << " ";
        return out;
    }

int main()
{
    VectorInt v1(2);
    v1.pushBack(1);
    cout << v1 << endl;
    v1.pushBack(2);
    cout << v1 << endl;
    v1.pushBack(3);
    cout << v1 << endl;
    v1.pushBack(4);
    cout << v1 << " " << endl;
    v1.popBack();
    cout << v1 << endl;
    v1.popBack();
    cout << v1 << endl;
    v1.shrinkToFit();
    cout << v1 << endl;
    v1.clear();
    cout << v1 << endl;
    v1.shrinkToFit();
    cout << v1.capacity()<< endl;
    v1.pushBack(1);
    v1.pushBack(2);
    cout << v1 << endl;
    v1.popBack();
    cout << v1.capacity() << endl;
    return 0;
}
