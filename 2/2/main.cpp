/*main.cpp*/
/*Program demonstrujący możliwości
klasy Point2d.*/
#include <iostream>
#include "point2d.hpp"

using namespace std;

int main()
{
    Point2d point(3, 4);
    Point2d point2(5, 0);

    cout << point << endl;
    point.jednokladnosc(5);
    cout << point << endl << endl;

    cout << point2 << endl;
    point2.obrot(3.1415/2); // o 90 stopni
    cout << point2 << endl << endl;

    cout << point2 << endl;
    point2.obrot(3.1415/2); // o 90 stopni
    cout << point2 << endl << endl;

    return 0;
}

