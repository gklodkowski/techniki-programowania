#include <iostream>
#include <math.h>
using namespace std;

class Figure {
public:
    float a= 0, b = 0 ,c = 0;
    string name;
    virtual double area() = 0;
    friend ostream& operator<<(ostream& out, Figure& in);
};
ostream& operator<<(ostream& out, Figure& in) {
        return out << in.name;
}
class Circle
        : public Figure {
public:
    Circle(float x) {
        a = x;
        name = "kolo";
    }
    virtual double area() {
        return 3.14*a*a;
    }
};
class Triangle
        : public Figure {
public:
    Triangle(float x, float y, float z) {
        a = x;
        b = y;
        c = z;
        name = "trojkat";
    }
    virtual double area() {
        float temp = (a+b+c)/2;
        return sqrt(temp*(temp-a)*(temp-b)*(temp-c));
    }
};
class Rectangle
        : public Figure {
public:
    Rectangle(float x, float y) {
        name = "prostokat";
        a = x;
        b = y;
    }
    virtual double area() {
        return a*b;
    }
};

int main()
{
    Figure * a = new Circle(1);
    Figure * b = new Triangle(3, 4, 5);
    Figure * c = new Rectangle(2, 3);
    cout << *a <<" o polu "<< a->area() << endl;
    cout << *b <<" o polu "<< b->area() << endl;
    cout << *c <<" o polu "<< c->area() << endl;
    delete a;
    delete b;
    delete c;

    return 0;
}
