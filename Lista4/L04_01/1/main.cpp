#include <iostream>

using namespace std;

class ComplexNumbers {
public:
    float imaginaryPart;
    float realPart;

    ComplexNumbers() {
        imaginaryPart = 0;
        realPart = 0;
    }
    ComplexNumbers(float real) {
        realPart = real;
        imaginaryPart = 0;
    }
    ComplexNumbers(float real, float imaginary) {
        realPart = real;
        imaginaryPart = imaginary;
    }
    //~ComplexNumbers();

    void printComplex() {
        if(this->imaginaryPart >= 0)
            cout << this->realPart << " + " << this->imaginaryPart << "i" << endl;
        else
            cout << this->realPart << " - " << (-1)*this->imaginaryPart << "i" << endl;
    }
    ComplexNumbers operator+(const ComplexNumbers& other) {
        this->realPart += other.realPart;
        this->imaginaryPart += other.imaginaryPart;
        this->printComplex();
    }
    ComplexNumbers operator-(const ComplexNumbers& other) {
        this->realPart -= other.realPart;
        this->imaginaryPart -= other.imaginaryPart;
        this->printComplex();
    }
    ComplexNumbers operator*(const ComplexNumbers& other) {
        float tempRel = (this->realPart)*other.realPart-(this->imaginaryPart)*other.imaginaryPart,
              tempIm = (this->imaginaryPart)*other.realPart+(this->realPart)*other.imaginaryPart;
        cout << tempRel << " + " << tempIm << "i" << endl;
    }
    ComplexNumbers operator/(const ComplexNumbers& other) {
        float tempRel = ((this->realPart)*other.realPart+(this->imaginaryPart)*other.imaginaryPart)/(other.realPart*other.realPart+other.imaginaryPart*other.imaginaryPart),
              tempIm = ((this->imaginaryPart)*other.realPart-(this->realPart)*other.imaginaryPart)/(other.realPart*other.realPart+other.imaginaryPart*other.imaginaryPart);
        cout << tempRel << " + " << tempIm << "i" << endl;
    }
    void conjugate() {
        this->imaginaryPart *= -1;
    }
    friend ostream& operator <<(ostream &out, const ComplexNumbers& in);
};
ostream& operator <<(ostream &out, const ComplexNumbers& in) {
        if(in.imaginaryPart >= 0)
            return out << in.realPart << " + " << in.imaginaryPart << "i";
        else
            return out << in.realPart << " - " << (-1)*in.imaginaryPart << "i";
}
int main()
{
    ComplexNumbers number1(1, 1);
    ComplexNumbers number2(2,7);
    number1-number2;
    cout << number1 << endl;
    //number2.conjugate();
    //number2.printComplex();
    return 0;
}
